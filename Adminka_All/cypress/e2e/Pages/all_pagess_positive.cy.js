import general_pom from "../POM/general_pom"
import refund_pom from "../POM/all_pages_pom";

//Adminka_Kacca cypress positive
//************************** Authorization **************************

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Login with correct credentials", () => {
    it("should successfully logined", () => {
        cy.login("medion_admin", "admin_medion123")
    })
})


//**************************** Возврат page ***********************************


describe("Positive cases Возврат", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.refund_icon().click()
    })
    it("Create new Возврат", () => {
        general_pom.table_buttons.add_button().click()
        refund_pom.elements.create_form_filial_input().click()
        general_pom.selectors.select_item("Udevs")
        refund_pom.elements.create_form_cassa_input().click()
        general_pom.selectors.select_item("For Full testing 123")
        refund_pom.elements.create_form_cassir_input().click()
        general_pom.selectors.select_item("Parfume Admin")
        refund_pom.elements.create_form_client_input().click()
        general_pom.selectors.select_item("Aziz PM")
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(2000)
        general_pom.table_buttons.inner_add_button().click()
        refund_pom.elements.refund_table_first_product_name_field().type("Refund #test")
        refund_pom.elements.refund_table_first_product_code_field().type('3760184353749')
        general_pom.selectors.select_item("3760184353749")
        refund_pom.elements.refund_table_first_product_type_field().click()
        general_pom.selectors.select_item("Брак")
        refund_pom.elements.refund_table_first_product_count_field().type("1")
        refund_pom.elements.refund_table_first_product_price_field().type("100000")
        refund_pom.elements.refund_inner_save_button().click()
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message("Успешно Обновлено")
    })
    it("Edit Возврат", () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(2000)
        refund_pom.elements.refund_table_first_item_old_client_field().click()
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Delete Возврат", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
    it("Open Возврат", () => {
        cy.wait(3000)
        general_pom.table_buttons.table_first_item().click({force: true})
        general_pom.errorCheckers.checkOpenedItemLabel('Филиал:')
    })

    //Возврат Продукты Тип page

    it("Create new Возврат продукт тип", () => {
        general_pom.table_buttons.add_button().click()
        refund_product_type_pom.elements.add_refund_product_type_name_input().type("test")
        refund_product_type_pom.elements.add_refund_product_type_slug_input().type("test")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Возврат продукт тип", () => {
        general_pom.table_buttons.edit_button().click()
        refund_product_type_pom.elements.edit_refund_product_type_table_first_item_name().clear().type('test #edited')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Возврат продукт тип", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Название:')
    })
    it("Delete Возврат продукт тип", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })

    //********************Инкассация page  ***************************

    it("Edit Инкассация", () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(3000)
        incassation_pom.elements.edit_incassation_table_first_item_cassa_clear_button().click({force: true})
        incassation_pom.elements.cassa_list_button().click()
        general_pom.selectors.select_item('For Full testing')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Инкассация", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Ид инкасации:')
    })
    it("Delete Инкассация", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })

    //******************** Касса тип оплаты page  *******************************/

    it("Create new Касса тип оплаты", () => {
        general_pom.table_buttons.add_button().click()
        cy.wait(2000)
        cassa_type_payment.elements.add_form_type_input().type("test")
        cassa_type_payment.elements.add_form_slug_input().type("test")
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message().should('have.text', "Успешно обновлено")
    })
    it("Edit Касса тип оплаты", () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(2000)
        cassa_type_payment.elements.slug_item_table().clear()
        cassa_type_payment.elements.slug_item_table().type('test #edit')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Delete Касса тип оплаты", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
    it("Open Касса тип оплаты", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel("Тип:")
    })


    //********************** Касса Транзакции  ******************************/

    it("Create new Касса Транзакции", () => {
        general_pom.table_buttons.add_button().click()
        cassa_transactions_pom.elements.create_filial_field().click()
        general_pom.selectors.select_item("Udevs")
        cassa_transactions_pom.elements.cassa_type_transaction_field().click()
        general_pom.selectors.select_item("Возврат")
        cassa_transactions_pom.elements.cassa_type_payment_field().click()
        general_pom.selectors.select_item("test")
        cassa_transactions_pom.elements.users().click()
        general_pom.selectors.select_item("Parfume Admin")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Касса Транзакции", () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(7000)
        cassa_transactions_pom.elements.table_first_item_type_transactions_list_button().click()
        general_pom.selectors.select_item("Продажа")
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Касса Транзакции", () => {
        general_pom.table_buttons.table_first_item().click()
        cassa_transactions_pom.elements.opened_item_header().contains("Номер транзакции:")
    })
    it("Delete Касса Транзакции", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
    it("Filter Касса Транзакции", () => {
        cassa_transactions_pom.elements.filter_by_id_field().click()
        cassa_transactions_pom.elements.filter_by_id_input().type("B-0000003")
        general_pom.selectors.select_item("B-0000003")
        cy.esc()
        cassa_transactions_pom.elements.first_item_cassa_id().should("have.text", "B-0000003")
    })

    //*************************** Клиенты page  *******************************/

    it("Create new Клиенты", () => {
        general_pom.table_buttons.add_button().click()
        clients_pom.elements.add_client_fio_input().type("Testerov Tester Testerovich")
        clients_pom.elements.add_client_phone_number_input().type("000000000")
        clients_pom.elements.add_client_birth_date_input().type('0').type('6').type('0').type('2').type('2').type('0').type('2').type('3').type('{enter}')
        clients_pom.elements.add_client_gender_input().click()
        general_pom.selectors.select_item("Мужчина")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Клиенты", () => {
        general_pom.table_buttons.edit_button().click()
        clients_pom.elements.edit_table_first_item_fio_input().clear().type("Testerov Tester #2 Testerovich")
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Клиенты", () => {
        general_pom.table_buttons.table_first_item().click()
        clients_pom.elements.opened_item_header().should('have.text', '  Ф.И.О:')
    })
    it("Delete Клиенты", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })

    //*************************** Консультант page  *******************************/

    it("Create new Консультант", () => {
        general_pom.table_buttons.add_button().click()
        consultants_pom.elements.add_consultant_lname_input().type('Testerov', {force: true})
        consultants_pom.elements.add_consultant_fname_input().type('Tester', {force: true})
        consultants_pom.elements.add_consultant_dad_name_input().type('Testerovich', {force: true})

        // ? Select Date of birth
        consultants_pom.elements.add_consultant_date_of_birth_input().type('0', {force: true}).type('6', {force: true}).type('0', {force: true}).type('2', {force: true}).type('2',{force: true}).type('0', {force: true}).type('2', {force: true}).type('3', {force: true}).type('{enter}', {force: true})

        consultants_pom.elements.add_consultant_filial_input().click()
        general_pom.selectors.select_item('Udevs'),
        consultants_pom.elements.add_consultant_gender_input().click()
        general_pom.selectors.select_first()
        consultants_pom.elements.add_consultant_address_input().type('Test Living City', {force: true})
        consultants_pom.elements.add_consultant_phone_number_input().type('998000000000', {force: true})
        consultants_pom.elements.add_consultant_phone_number_additional_input().type('998000000000', {force: true})
        consultants_pom.elements.add_consultant_pinfl_input().type('00000000000000', {force: true})
        consultants_pom.elements.add_consultant_passport_seria_input().type('XX', {force: true})
        consultants_pom.elements.add_consultant_passport_number_input().type('0000000', {force: true})

        // ? Select Passport Expire Date
        consultants_pom.elements.add_consultant_passport_expire_date_input().type('0', {force: true}).type('6', {force: true}).type('0', {force: true}).type('2', {force: true}).type('2',{force: true}).type('0', {force: true}).type('2', {force: true}).type('3', {force: true}).type('{enter}', {force: true})
        
        consultants_pom.elements.add_consultant_status_input().click()
        general_pom.selectors.select_first()
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Консультант", () => {
        general_pom.table_buttons.edit_button().click()
        consultants_pom.elements.edit_consultant_table_first_item_passport_seria().clear().type('XZ', {force: true})
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Консультант", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel("Фото:")
    })
    it("Delete Консультант", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })

    //*************************** Приемка Денег page  *******************************/

    it("Create new Приемка денег", () => {
        general_pom.table_buttons.add_button().click()
        accept_money_pom.elements.add_new_filial_input().click()
        general_pom.selectors.select_item("Udevs")
        accept_money_pom.elements.cassa_point_input().click()
        general_pom.selectors.select_item("For Full testing")
        accept_money_pom.elements.worker_input().click()
        general_pom.selectors.select_item("Test")
        accept_money_pom.elements.label_input().type('#Testing')
        accept_money_pom.elements.summ_input().type('500000')
        accept_money_pom.elements.smena_input().click()
        general_pom.selectors.select_item("K-000")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Приемка денег", () => {
        general_pom.table_buttons.edit_button().click()
        accept_money_pom.elements.table_first_item_label().clear().type('edited #2')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Приемка денег", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel("Филиал:")
    })
    it("Delete Приемка денег", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })

    //*************************** Продажа page  *******************************/

    it("Create Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.open_smena() 
    })
    it("Create new Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.add_button().click()
        cy.wait(2000)
        cy.get('._mainCardSide_14nop_9').scrollTo('top', {ensureScrollable: false})
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(5000)
        prodaja_pom.elements.product_code_field().type("3760184353749", {force: true})
        cy.wait(2000)
        prodaja_pom.elements.payment_button().click()
        cy.wait(2000)
        prodaja_pom.elements.inner_add_button().click()
        cy.wait(5000)
        prodaja_pom.elements.inner_save_button().click()
        cy.wait(5000)
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(5000)
    })
    it("Edit Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.edit_button().click()
        cy.wait(1000)
        cy.get('.CTableBody').scrollTo('top', {ensureScrollable: false})
        prodaja_pom.elements.client_list_button().click()
        cy.wait(1000)
        prodaja_pom.elements.first_client_on_list().click()
        cy.wait(1000)
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.table_first_item().click()
        cy.wait(2000)
        prodaja_pom.elements.opened_item_header().should('have.text', '  Штрихкод:')
    })
    it("Delete Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.table_first_item_delete_button().click({force: true})
    })
    it("Filter Продажа", () => {
        cy.wait(2000)
        prodaja_pom.elements.filter_by_filial_input().click()
        cy.wait(2000)
        prodaja_pom.elements.select_filial("Udevs").click()
        cy.esc()
        prodaja_pom.elements.first_item_filial().contains("Udevs")
        prodaja_pom.elements.filter_by_smena_input().type('K-0000001')
        general_pom.selectors.select_item('K-0000001')
        cy.esc()
        prodaja_pom.elements.first_item_smena().contains('K-0000001')
    })
    it("Delete Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.wait(5000)
        cy.get(':nth-child(1) > [style="padding: 0px;"]').click()
    })




})