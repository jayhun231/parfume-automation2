Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import accept_money_pom from "../../POM/accept_money_pom";

describe("Positive cases Приемка денег", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.accept_money_icon().click()
    })
    it.only("Create new Приемка денег", () => {
        general_pom.table_buttons.add_button().click()
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Приемка денег", () => {
        general_pom.table_buttons.edit_button().click()
        accept_money_pom.elements.table_first_item_label().clear()
        general_pom.table_buttons.edit_save_button().click()
    })
})