Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import accept_money_pom from "../../POM/accept_money_pom";

describe("Positive cases Приемка денег", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.accept_money_icon().click()
    })
    it("Create new Приемка денег", () => {
        general_pom.table_buttons.add_button().click()
        accept_money_pom.elements.add_new_filial_input().click()
        general_pom.selectors.select_item("Udevs")
        accept_money_pom.elements.cassa_point_input().click()
        general_pom.selectors.select_item("For Full testing")
        accept_money_pom.elements.worker_input().click()
        general_pom.selectors.select_item("Test")
        accept_money_pom.elements.label_input().type('#Testing')
        accept_money_pom.elements.summ_input().type('500000')
        accept_money_pom.elements.smena_input().click()
        general_pom.selectors.select_item("K-000")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Приемка денег", () => {
        general_pom.table_buttons.edit_button().click()
        accept_money_pom.elements.table_first_item_label().clear().type('edited #2')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Приемка денег", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel("Филиал:")
    })
    it("Delete Приемка денег", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
})