Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import clients_pom from "../../POM/clients_pom";

describe("Positive cases Клиенты", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.clients_icon().click()
    })
    it("Create new Клиенты", () => {
        general_pom.table_buttons.add_button().click()
        general_pom.table_buttons.add_form_submit_button().click()
        clients_pom.elements.validation_error_text().should('have.text', 'This is required field')
    })
    it("Edit Клиенты", () => {
        general_pom.table_buttons.edit_button().click()
        clients_pom.elements.edit_table_first_item_fio_input().clear().type("             ")
        general_pom.table_buttons.edit_save_button().click()
    })
})