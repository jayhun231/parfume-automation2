Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import clients_pom from "../../POM/clients_pom";

describe("Positive cases Клиенты", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.clients_icon().click()
    })
    it("Create new Клиенты", () => {
        general_pom.table_buttons.add_button().click()
        clients_pom.elements.add_client_fio_input().type("Testerov Tester Testerovich")
        clients_pom.elements.add_client_phone_number_input().type("000000000")
        clients_pom.elements.add_client_birth_date_input().type('0').type('6').type('0').type('2').type('2').type('0').type('2').type('3').type('{enter}')
        clients_pom.elements.add_client_gender_input().click()
        general_pom.selectors.select_item("Мужчина")
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Клиенты", () => {
        general_pom.table_buttons.edit_button().click()
        clients_pom.elements.edit_table_first_item_fio_input().clear().type("Testerov Tester #2 Testerovich")
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Клиенты", () => {
        general_pom.table_buttons.table_first_item().click()
        clients_pom.elements.opened_item_header().should('have.text', '  Ф.И.О:')
    })
    it("Delete Клиенты", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
})