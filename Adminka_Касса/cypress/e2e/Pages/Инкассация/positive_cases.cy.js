Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import incassation_pom from "../../POM/incassation_pom";

describe("Positive cases Инкассация", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.incassation_icon().click()
    })
    it("Edit Инкассация", () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(3000)
        incassation_pom.elements.edit_incassation_table_first_item_cassa_clear_button().click({force: true})
        incassation_pom.elements.cassa_list_button().click()
        general_pom.selectors.select_item('For Full testing')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Инкассация", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Ид инкасации:')
    })
    it("Delete Инкассация", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
})