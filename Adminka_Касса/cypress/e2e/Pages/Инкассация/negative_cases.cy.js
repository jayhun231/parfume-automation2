Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import incassation_pom from "../../POM/incassation_pom";

describe("Negative cases Инкассация", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.incassation_icon().click()
    })
    it("Edit Инкассация", () => {
        general_pom.table_buttons.edit_button().click()
        incassation_pom.elements.edit_incassation_table_first_item_cassa_clear_button().click({force: true})
        general_pom.table_buttons.edit_save_button().click()
    })
})