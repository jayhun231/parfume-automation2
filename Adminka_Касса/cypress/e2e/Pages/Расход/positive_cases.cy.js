Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import consumption_pom from "../../POM/consumption_pom";

describe("Positive cases Расход", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.consumption_sidebar_icon().click()
    })
    it("Create Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.open_smena() 
    })
    it("Add new Расход", () => {
        general_pom.table_buttons.add_button().click()
        consumption_pom.elements.filial_field().click({force: true})
        general_pom.selectors.select_item("Udevs")
        consumption_pom.elements.cassa_field().click({force: true})
        general_pom.selectors.select_item("For Full")
        consumption_pom.elements.consumption_type_field().click({force: true})
        general_pom.selectors.select_item("обед")
        consumption_pom.elements.payment_type_field().type("Наличные")
        general_pom.selectors.select_item("Наличные")
        consumption_pom.elements.payment_summ_field().type('100')
        general_pom.table_buttons.add_form_submit_button().click()
        consumption_pom.elements.first_item_table().click()
        cy.wait(2000)
        consumption_pom.elements.confirm_consumption_button().click()
    })
    it("Edit Расход", () => {
        general_pom.table_buttons.edit_button().click()
        consumption_pom.elements.table_consumption_type_field_button().click()
        general_pom.selectors.select_item("офис")
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Расход details page", () => {
        general_pom.table_buttons.table_first_item().click()
        consumption_pom.elements.opened_item_header().contains("Расход-ID:")
    })
    it("Delete Расход", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
    it("Filter Расход", () => {
        consumption_pom.elements.filter_by_filial_input().click()
        general_pom.selectors.select_item("Parfume Admin")
        cy.esc()
        consumption_pom.elements.table_first_item_cassir().contains("Parfume Admin")
    })
    it("Delete Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.wait(5000)
        cy.get(':nth-child(1) > [style="padding: 0px;"]').click()
    })
})