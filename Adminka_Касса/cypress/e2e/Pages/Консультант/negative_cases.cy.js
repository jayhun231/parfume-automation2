Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import consultants_pom from "../../POM/consultants_pom";

describe("Positive cases Консультант", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.consultant_icon().click()
    })
    it("Create new Консультант", () => {
        general_pom.table_buttons.add_button().click()
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Консультант", () => {
        general_pom.table_buttons.edit_button().click()
        consultants_pom.elements.edit_consultant_table_first_item_passport_seria().clear()
        general_pom.table_buttons.edit_save_button().click()
    })
})