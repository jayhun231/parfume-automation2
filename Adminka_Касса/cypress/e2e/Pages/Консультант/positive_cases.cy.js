Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import consultants_pom from "../../POM/consultants_pom";

describe("Positive cases Консультант", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.consultant_icon().click()
    })
    it("Create new Консультант", () => {
        general_pom.table_buttons.add_button().click()
        consultants_pom.elements.add_consultant_lname_input().type('Testerov', {force: true})
        consultants_pom.elements.add_consultant_fname_input().type('Tester', {force: true})
        consultants_pom.elements.add_consultant_dad_name_input().type('Testerovich', {force: true})

        // ? Select Date of birth
        consultants_pom.elements.add_consultant_date_of_birth_input().type('0', {force: true}).type('6', {force: true}).type('0', {force: true}).type('2', {force: true}).type('2',{force: true}).type('0', {force: true}).type('2', {force: true}).type('3', {force: true}).type('{enter}', {force: true})

        consultants_pom.elements.add_consultant_filial_input().click()
        general_pom.selectors.select_item('Udevs'),
        consultants_pom.elements.add_consultant_gender_input().click()
        general_pom.selectors.select_first()
        consultants_pom.elements.add_consultant_address_input().type('Test Living City', {force: true})
        consultants_pom.elements.add_consultant_phone_number_input().type('998000000000', {force: true})
        consultants_pom.elements.add_consultant_phone_number_additional_input().type('998000000000', {force: true})
        consultants_pom.elements.add_consultant_pinfl_input().type('00000000000000', {force: true})
        consultants_pom.elements.add_consultant_passport_seria_input().type('XX', {force: true})
        consultants_pom.elements.add_consultant_passport_number_input().type('0000000', {force: true})

        // ? Select Passport Expire Date
        consultants_pom.elements.add_consultant_passport_expire_date_input().type('0', {force: true}).type('6', {force: true}).type('0', {force: true}).type('2', {force: true}).type('2',{force: true}).type('0', {force: true}).type('2', {force: true}).type('3', {force: true}).type('{enter}', {force: true})
        
        consultants_pom.elements.add_consultant_status_input().click()
        general_pom.selectors.select_first()
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Консультант", () => {
        general_pom.table_buttons.edit_button().click()
        consultants_pom.elements.edit_consultant_table_first_item_passport_seria().clear().type('XZ', {force: true})
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Консультант", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel("Фото:")
    })
    it("Delete Консультант", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
})