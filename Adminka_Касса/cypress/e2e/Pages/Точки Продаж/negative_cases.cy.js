Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import sellment_point from "../../POM/sellment_point";

describe("Negative cases Точки Продаж", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.sellment_point_icon().click()
    })
    it("Create new Точки Продаж", () => {
        general_pom.table_buttons.add_button().click()
        sellment_point.elements.add_point_name_input().type("          ")
        sellment_point.elements.add_point_filial_input().click()
        general_pom.selectors.select_item('Udevs')
        general_pom.table_buttons.add_form_submit_button().click()
    })
    it("Edit Точки Продаж", () => {
        general_pom.table_buttons.edit_button().click()
        sellment_point.elements.edit_point_table_first_item_name().clear()
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Filter", () => {
        sellment_point.elements.filial_selector().click()
        general_pom.selectors.select_item('Udevs')
        general_pom.selectors.select_item('Santini')
        cy.esc()
        sellment_point.elements.table_first_item_filial().should('have.text', 'Udevs ')
    })
})