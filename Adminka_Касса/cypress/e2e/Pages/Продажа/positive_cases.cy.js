Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});
import general_pom from "../../POM/general_pom";
import prodaja_pom from "../../POM/prodaja_pom";
describe("Positive cases Продажа", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.sellment_page_icon().click()
    })
    it("Create Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.open_smena() 
    })
    it("Create new Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.add_button().click()
        cy.wait(2000)
        cy.get('._mainCardSide_14nop_9').scrollTo('top', {ensureScrollable: false})
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(5000)
        prodaja_pom.elements.product_code_field().type("3760184353749", {force: true})
        cy.wait(2000)
        prodaja_pom.elements.payment_button().click()
        cy.wait(2000)
        prodaja_pom.elements.inner_add_button().click()
        cy.wait(5000)
        prodaja_pom.elements.inner_save_button().click()
        cy.wait(5000)
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(5000)
    })
    it("Edit Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.edit_button().click()
        cy.wait(1000)
        cy.get('.CTableBody').scrollTo('top', {ensureScrollable: false})
        prodaja_pom.elements.client_list_button().click()
        cy.wait(1000)
        prodaja_pom.elements.first_client_on_list().click()
        cy.wait(1000)
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.table_first_item().click()
        cy.wait(2000)
        prodaja_pom.elements.opened_item_header().should('have.text', '  Штрихкод:')
    })
    it("Delete Продажа", () => {
        cy.wait(2000)
        general_pom.table_buttons.table_first_item_delete_button().click({force: true})
    })
    it("Filter Продажа", () => {
        cy.wait(2000)
        prodaja_pom.elements.filter_by_filial_input().click()
        cy.wait(2000)
        prodaja_pom.elements.select_filial("Udevs").click()
        cy.esc()
        prodaja_pom.elements.first_item_filial().contains("Udevs")
        prodaja_pom.elements.filter_by_smena_input().type('K-0000001')
        general_pom.selectors.select_item('K-0000001')
        cy.esc()
        prodaja_pom.elements.first_item_smena().contains('K-0000001')
    })
    it("Delete Smena", () => {
        general_pom.sidebar_icons.smena_sidebar_icon().click()
        cy.wait(5000)
        cy.get(':nth-child(1) > [style="padding: 0px;"]').click()
    })
})