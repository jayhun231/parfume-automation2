Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

import general_pom from "../../POM/general_pom"
import smena_pom from "../../POM/smena_pom";

describe("Positive cases Смена", () => {
    beforeEach("Login", () => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        cy.cassa()
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item("Касса")
        general_pom.sidebar_icons.smena_sidebar_icon().click()
    })
    it("Create new Смена and Open it", () => {
        general_pom.table_buttons.add_button().click()
        cy.wait(3000)
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(1000)
        smena_pom.elements.open_smena_button().click()
    })
    it("Open Смена details", () => {
        general_pom.table_buttons.table_first_item().click()
    })
    it("Close Смена function", () => {
        general_pom.table_buttons.table_first_item().click()
        cy.wait(5000)
        smena_pom.elements.close_smena_button().click()
    })
    it("Edit Смена", () => {
        general_pom.table_buttons.edit_button().click()
        smena_pom.elements.edit_cassir_open_list().click()
        general_pom.selectors.select_item("Windows 10 Linux")
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Delete Смена", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
    })
})