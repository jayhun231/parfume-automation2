const general_pom = require("../../POM/general_pom");
const movement_of_goods_pom = require("../../POM/brak_pom");
const brak_pom = require("../../POM/brak_pom");

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Движение товаров", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Заявки')
        general_pom.sidebar_icons.zay_per_icon().click()
    })

    it('Table column check', () => {
        brak_pom.elements.table_colum().should('have.text', 'Заявка-ID ')
        brak_pom.elements.table_column0().should('have.text','Филиал ')
        brak_pom.elements.table_column1().should('have.text','Пользователи ')
        brak_pom.elements.table_column2().should('have.text','Статус ')
    })

    it('ADD page', () => {
        general_pom.table_buttons.add_button().click()
        brak_pom.elements.add_input().click()
        general_pom.selectors.select_first()
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message('Успешно создан')
    })

    it('Edit page', () => {
        general_pom.table_buttons.edit_button().click()
        brak_pom.elements.edit_input().click()
        general_pom.selectors.select_first()
        general_pom.table_buttons.edit_save_button().click()
        general_pom.errorCheckers.alert_message('Успешно обновлен')
    })

    it('Table click', () => {
        brak_pom.elements.table_click().click()
        brak_pom.elements.add_input().click()
        general_pom.selectors.select_first()
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message('Успешно обновлен')
    })

    it('Delete button', () =>{
        general_pom.table_buttons.table_first_item_delete_button().click()
        general_pom.errorCheckers.alert_message('Успешно обновлен')
    })

})