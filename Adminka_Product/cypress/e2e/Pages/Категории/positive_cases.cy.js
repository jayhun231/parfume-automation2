const general_pom = require("../../POM/general_pom");
const fillial_pom = require("../../POM/fillial_pom");
const kart_pom = require("../../POM/kart_pom");
const product_pom = require("../../POM/katego_pom");
const katego_pom = require("../../POM/katego_pom");

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Возврат перемещения-отправка", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Товары')
        general_pom.sidebar_icons.kategor_icone().click()
    })

    it('Add category', () => {
        general_pom.table_buttons.add_button().click()
        katego_pom.elements.add_input().click().type('submit')
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message('Успешно создан')

    })

    it('Edit', () => {
        cy.get(':nth-child(1) > ._title_t60r2_8').click()
        cy.get('.MuiCollapse-wrapperInner > :nth-child(1)').click()
        cy.get('._formColumn_14nop_27 > :nth-child(3)').find('input').first().click()
        general_pom.selectors.select_item('Test Brand')
        general_pom.table_buttons.add_form_submit_button().click()
        general_pom.errorCheckers.alert_message('Успешно обновлен')

    })

    it('Delete', () => {
        cy.get(':nth-child(24) > ._extra_t60r2_11 > .error').click()
        
    })
    


})