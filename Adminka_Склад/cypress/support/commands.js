//! <============ Requirements ============>
import general_pom from "../e2e/POM/general_pom"
import moving_sending_pom from "../e2e/POM/moving_sending_pom"
import inventarization_pom from "../e2e/POM/inventarization_pom"

//? <============ Login ============>
Cypress.Commands.add('login', (login, password) => {
    // cy.session("Logged User", () => {
    cy.visit("https://test.admin.parfumgallery.uz/login")
    cy.get('._formArea_35dot_54').find('input').first().type(login)
    cy.get('._formArea_35dot_54').find('input').eq(1).type(password)
    cy.get('._button_19zv0_1').click()
    // })
})

//todo <============ Custom Commands ============>
Cypress.Commands.add('pastle', (elem, text) => {
    elem.type(text, { delay: 0 })
})
Cypress.Commands.add('esc', () => {
    cy.get('body').trigger('keydown', { keyCode: 27 });
    cy.wait(500);
    cy.get('body').trigger('keyup', { keyCode: 27 });
})
Cypress.Commands.add('enter', () => {
    cy.get('body').trigger('keydown', { keyCode: 13 });
    cy.wait(500);
    cy.get('body').trigger('keyup', { keyCode: 13 });
})

//* <============ Pre-condition Commands ============>
Cypress.Commands.add('moving_send', (code) => {
    general_pom.sidebar_icons.moving_sending_icon().click()
    cy.wait(2000)
    general_pom.table_buttons.add_button().click()
    cy.wait(2000)
    general_pom.table_buttons.add_form_submit_button().click()
    cy.wait(2000)
    moving_sending_pom.elements.add_product_from_filial().click()
    general_pom.selectors.select_item('Udevs')
    moving_sending_pom.elements.add_product_to_filial().click()
    general_pom.selectors.select_item('Santini')
    moving_sending_pom.elements.add_product_submit_button().click()
    cy.wait(2000)
    moving_sending_pom.elements.add_product_code_input().type(code)
    cy.wait(1000)
    moving_sending_pom.elements.add_product_send_button().click()
    cy.wait(2000)
    general_pom.errorCheckers.checkErrorNotiflicationMessage('Перемещение-Отправка успешно сделана!')
})
Cypress.Commands.add('inventarization_of_product', (code, element) => {
    cy.wait(2000)
    const productCode = code
    element.then($value => {
        const textValue = $value.text()

        for (let i = 1; i < Number(textValue); i++) {
            cy.wait(2000)
            cy.pastle(inventarization_pom.elements.add_inventarization_product_code_input(), productCode)
            general_pom.errorCheckers.checkErrorNotiflicationMessage('Успешно обновлено!')
        }
    })
    cy.wait(2000)
})