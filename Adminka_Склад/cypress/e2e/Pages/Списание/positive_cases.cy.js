const write_off_pom = require("../../POM/write_off_pom");
const general_pom = require("../../POM/general_pom");


Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Списание", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Склад')
        general_pom.sidebar_icons.write_off_icon().click()
    })
    it('Create new Списание', () => {
        general_pom.table_buttons.add_button().click()
        write_off_pom.elements.add_write_off_filial_input().click()
        general_pom.selectors.select_item('Uacademy')
        write_off_pom.elements.add_write_off_type_input().type('Списание')
        general_pom.selectors.select_item('Списание')
        write_off_pom.elements.add_write_off_users_input().click()
        general_pom.selectors.select_item('Boburbek')
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(2000)
        cy.pastle(write_off_pom.elements.add_write_off_product_code_input(), '725840312068')
        cy.wait(2000)
        write_off_pom.elements.write_off_product_button().click()
        cy.wait(1000)
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Списание сделано')
    })
    it('Edit Списание', () => {
        general_pom.table_buttons.edit_button().click()
        cy.wait(2000)
        write_off_pom.elements.edit_write_off_table_first_item_type_clear_button().click({force: true})
        write_off_pom.elements.edit_write_off_table_first_item_type().click()
        general_pom.selectors.select_item('Расход')
        general_pom.table_buttons.edit_save_button().click()
    })
    it('Open Списание', () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Штрих-код:')
    })
    it('Filter By Склад', () => {
        write_off_pom.elements.filter_by_sklad().click()
        general_pom.selectors.select_item('Uacademy')
        cy.esc()
        write_off_pom.elements.founded_items_first_sklad().should('have.text', 'Uacademy ')
    })
    it('Filter By Type', () => {
        write_off_pom.elements.filter_by_type().click()
        general_pom.selectors.select_item('Указать')
        cy.esc()
        write_off_pom.elements.founded_items_first_type().should('have.text', 'Указать сотрудника')
    })
    it('Delete Списание', () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Успешно обновлено!')
    })
})