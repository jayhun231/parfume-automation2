const general_pom = require("../../POM/general_pom");
const moving_sending_pom = require("../../POM/moving_sending_pom");

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Перемещение Отправка", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Склад')
        general_pom.sidebar_icons.moving_sending_icon().click()
    })
    it.only("Create Перемещение Отправка", () => {
        cy.wait(2000)
        general_pom.table_buttons.add_button().click()
        cy.wait(2000)
        general_pom.table_buttons.add_form_submit_button().click()
        cy.wait(2000)
        moving_sending_pom.elements.add_product_from_filial().click()
        general_pom.selectors.select_item('Udevs')
        moving_sending_pom.elements.add_product_to_filial().click()
        general_pom.selectors.select_item('Santini')
        moving_sending_pom.elements.add_product_submit_button().click()
        cy.wait(2000)
        moving_sending_pom.elements.add_product_code_input().type("C-00000017")
        cy.wait(1000)
        moving_sending_pom.elements.add_product_send_button().click()
        cy.wait(2000)
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Перемещение-Отправка успешно сделана!')
    })
    it("Edit Перемещение Отправка", () => {
        general_pom.table_buttons.edit_button().click()
        moving_sending_pom.elements.edit_product_to_filial_clear_button().click({force: true})
        moving_sending_pom.elements.edit_product_to_filial_list_input().type("Santini")
        general_pom.selectors.select_item('Santini')
        general_pom.table_buttons.edit_save_button().click()
    })
    it("Open Перемещение Отправка", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Штрихкод:')
    })
    it("Delete Перемещение Отправка", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Успешно обновлено!')
    })
})