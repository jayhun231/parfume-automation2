const general_pom = require("../../POM/general_pom");
const moving_shipping_pom = require("../../POM/moving_shipping_pom");


Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Перемещение Принятие", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Склад')
        general_pom.sidebar_icons.moving_shipping_icon().click()
    })
    it("Принять Перемещение Принятие", () => {
        const productCode = "C-00000017"

        //! Create New Moving-Sending
        cy.moving_send(productCode)

        general_pom.sidebar_icons.moving_shipping_icon().click()
        general_pom.table_buttons.table_first_item().click()
        cy.pastle(moving_shipping_pom.elements.add_new_form_product_code_input(), productCode)
        cy.wait(2000)
        moving_shipping_pom.elements.accept_moving_shipping_button().click()
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Перемещение-Отправка успешно сделана!')
    })
    it("Open Перемещение Принятие", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.checkOpenedItemLabel('Штрихкод:')
    })
    it("Delete Перемещение Принятие", () => {
        general_pom.table_buttons.table_first_item_delete_button().click()
        general_pom.errorCheckers.checkErrorNotiflicationMessage('Успешно обновлено!')
    })
})