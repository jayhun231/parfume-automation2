const general_pom = require("../../POM/general_pom");
const remainder_pom = require("../../POM/remainder_pom");

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
});

describe("Positive cases Остаток", () => {
    beforeEach(() => {
        cy.viewport(1700, 700)
        cy.login("medion_admin", "admin_medion123")
        general_pom.sidebar_icons.module_button().click()
        general_pom.selectors.select_item('Склад')
        general_pom.sidebar_icons.remainder_icon().click()
    })
    it("Open Остаток", () => {
        general_pom.table_buttons.table_first_item().click()
        general_pom.errorCheckers.opened_item_label('Поставщики:')
    })
    it("Filter Остаток by filial", () => {
        remainder_pom.elements.filter_by_filial_input().click()
        general_pom.selectors.select_item('Udevs')
        cy.esc()
    })
    it("Filter Остаток by product", () => {
        remainder_pom.elements.filter_by_product_input().click()
        general_pom.selectors.select_item('сертиф200')
        cy.esc()
    })
    it("Filter Остаток by category", () => {
        remainder_pom.elements.filter_by_category_input().click()
        general_pom.selectors.select_item('Сертификаты')
        cy.esc()
    })
    it("Filter Остаток by product code", () => {
        remainder_pom.elements.filter_by_product_code_input().click()
        general_pom.selectors.select_item('3700550220480')
        cy.esc()
    })
})